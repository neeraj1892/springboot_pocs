package com.iris.employee;

import feign.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableSwagger2
public class EmployeeServiceApplication {

    @Value("${feign.request.period}")
    private int readTimeOutMillis;

    @Value("${feign.request.max.period}")
    private int connectionTimeoutMillis;

    public static void main(String[] args) {
        SpringApplication.run(EmployeeServiceApplication.class, args);
    }

    /*
    Bean Configuration for Message Source to pickup messages from message.properties file.
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:message");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }

    /*
    Bean configuration for Swagger Documentation
     */
    @Bean
    public Docket swaggerPersonApi10() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.iris.employee.controller"))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/api")
                .apiInfo(new ApiInfoBuilder().version("1.0").title("IRIS Employee API").description("Documentation Employee Service API").build());
    }

    /**
     * Timeout configuration for feign Client
     *
     * @return
     */

    @Bean
    Request.Options requestOptions() {

        return new Request.Options(connectionTimeoutMillis, readTimeOutMillis);
    }

}
