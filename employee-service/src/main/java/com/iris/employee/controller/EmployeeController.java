package com.iris.employee.controller;

import com.iris.employee.handler.InvalidArgumentException;
import com.iris.employee.manager.EmployeeManager;
import com.iris.employee.model.Employee;
import com.iris.employee.validator.EmployeeValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

import static com.iris.employee.constants.EmployeeConstants.INVALID_ID;

@RestController
public class EmployeeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    private EmployeeManager employeeManager;
    private EmployeeValidator employeeValidator;
    private MessageSource messageSource;

    public EmployeeController(EmployeeManager employeeManager, EmployeeValidator employeeValidator, MessageSource messageSource) {
        this.employeeManager = employeeManager;
        this.employeeValidator = employeeValidator;
        this.messageSource = messageSource;
    }

    /**
     * Find Employee details by id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Employee findById(@PathVariable Long id){
        checkIfValidId(id);
        Employee employee = employeeManager.findById(id);
        LOGGER.info("Employee find : {}", employee);
        return employee;
    }

    /**
     * Find all employees details available in system.
     * @return
     */
    @GetMapping("/")
    public List<Employee> findAllEmployees(){
        LOGGER.info("Finding all employees");
        return employeeManager.findAllEmployee();
    }

    /**
     * Create details for a new employee
     * @param employee
     * @return
     */
    @PostMapping("/")
    public Employee createEmployee(@Valid @RequestBody Employee employee){
        LOGGER.info("Create Employee: {}", employee);
        return employeeManager.createEmployee(employee);
    }

    /**
     * Delete employee details by Id
     * @param id
     */
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        checkIfValidId(id);
        LOGGER.info("Delete Employee with Id: {}", id);
        employeeManager.deleteById(id);
    }

    /**
     * Update employee details based on its ID.
     * @param employee
     * @param id
     * @return
     */
    @PutMapping("/{id}")
    public Employee updateEmployee(@Valid @RequestBody Employee employee, @PathVariable Long id){
        checkIfValidId(id);
        LOGGER.info("Update employee with Id :{}", id);
        return employeeManager.updateEmployee(employee, id);
    }

    /**
     * Checks if a received Id is valid or not
     * If received Id is invalid then throw error.
     * @param id
     */
    private void checkIfValidId(Long id) {
        if(employeeValidator.validateId(id)){
            LOGGER.error("Invalid Id {} received.", id);
            throw new InvalidArgumentException(getMessage(INVALID_ID));
        }
    }

    private String getMessage(String messageKey){
        return messageSource.getMessage(messageKey, null, Locale.FRANCE);
    }

}
