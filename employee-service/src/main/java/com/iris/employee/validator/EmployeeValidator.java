package com.iris.employee.validator;

import org.springframework.stereotype.Component;

@Component
public class EmployeeValidator {

    public boolean validateId(Long id){
        return id < 0;
    }

}
