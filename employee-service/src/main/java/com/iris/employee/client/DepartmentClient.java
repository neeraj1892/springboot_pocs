package com.iris.employee.client;

import com.iris.employee.model.Department;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "DEPARTMENT-SERVICE")
public interface DepartmentClient {
    @GetMapping("/{id}")
    public Department findById(@PathVariable Long id);
}
