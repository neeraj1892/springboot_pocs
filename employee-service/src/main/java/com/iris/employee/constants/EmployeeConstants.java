package com.iris.employee.constants;

public class EmployeeConstants {

    public static final String EMPLOYEE_NOT_FOUND = "employee.not.found";
    public static final String VALIDATION_FAILED = "Validation Failed.";
    public static final String VALUE_SEPERATER = " : ";
    public static final String INVALID_ID = "invalid.id";

    public static final String INVALID_DEPARTMENT="department.invalid";
    public static final String VALIDATION_DEPARTMENT_ID_NOT_EMPTY = "{validation.department.id.notEmpty}";
    public static final String VALIDATION_DEPARTMENT_ID_NOT_NEGATIVE = "{validation.department.id.not.negative}";
    public static final String VALIDATION_FIRST_NAME_NOT_EMPTY = "{validation.employee.firstName.notEmpty}";
    public static final String VALIDATION_LAST_NAME_NOT_EMPTY = "{validation.employee.lastName.notEmpty}";
    public static final String VALIDATION_PHONE_NUMBER_NOT_EMPTY = "{validation.employee.phoneNumber.notEmpty}";
    public static final String VALIDATION_PHONE_NUMBER_SIZE = "{validation.employee.phoneNumber.size}";
    public static final String VALIDATION_ADDRESS_NOT_EMPTY = "{validation.employee.address.notEmpty}";
}
