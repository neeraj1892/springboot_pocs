package com.iris.employee.manager;

import com.iris.employee.client.DepartmentClient;
import com.iris.employee.handler.EmployeeNotFoundException;
import com.iris.employee.handler.InvalidDepartmentException;
import com.iris.employee.model.Department;
import com.iris.employee.model.Employee;
import com.iris.employee.repository.DepartmentRedisRepository;
import com.iris.employee.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.iris.employee.constants.EmployeeConstants.EMPLOYEE_NOT_FOUND;
import static com.iris.employee.constants.EmployeeConstants.INVALID_DEPARTMENT;

@Service
public class EmployeeManagerImpl implements EmployeeManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeManagerImpl.class);

    private EmployeeRepository employeeRepository;
    private MessageSource messageSource;
    private DepartmentClient departmentClient;
    private DepartmentRedisRepository departmentRedisRepository;

    public EmployeeManagerImpl(EmployeeRepository employeeRepository, MessageSource messageSource, DepartmentClient departmentClient, DepartmentRedisRepository departmentRedisRepository) {
        this.employeeRepository = employeeRepository;
        this.messageSource = messageSource;
        this.departmentClient = departmentClient;
        this.departmentRedisRepository = departmentRedisRepository;
    }

    /**
     * Create details for a new employee
     * @param employee
     * @return
     */
    @Override
    public Employee createEmployee(Employee employee){
        Optional<Department> d = departmentRedisRepository.findById(employee.getDepartmentId());
        if(!d.isPresent()){
            validateDepartmentIdFromClient(employee.getDepartmentId());
        }
        Employee createdEmployee = employeeRepository.save(employee);
        LOGGER.info("Employee details successfully created : {}", createdEmployee);
        return createdEmployee;
    }

    /**
     * Find details of a specific employee with Id
     * If not found then throw error.
     * @param id
     * @return
     */
    @Override
    public Employee findById(Long id){
        Optional<Employee> employee = employeeRepository.findById(id);
        verifyIfEmployeeIsPresent(employee, id);
        LOGGER.info("Employee details found for Id {} are : {}", id, employee.get());
        return employee.get();
    }

    /**
     * Fetches list of all available employees in DB.
     * @return
     */
    @Override
    public List<Employee> findAllEmployee(){
        LOGGER.info("Finding list of all employees");
        return employeeRepository.findAll();
    }

    /**
     * Delete details of an employee using it's Id . If employee details are
     * not found in DB then throw error.
     * @param id
     */
    @Override
    public void deleteById(Long id){
        Employee existingEmployee = findById(id);
        LOGGER.info("Deleting employee with ID {} with details : {}", id, existingEmployee);
        employeeRepository.deleteById(id);
    }

    /**
     * Update details for a employee based on its ID.
     * @param employee
     * @param id
     * @return
     */
    @Override
    public Employee updateEmployee(Employee employee, Long id){
        Employee existingEmployee = findById(id);
        /*
            If department id is being updated then verify it from department service first.
         */
        if(existingEmployee.getDepartmentId() != employee.getDepartmentId()){
            validateDepartmentIdFromClient(employee.getDepartmentId());
        }
        employee.setId(id);
        LOGGER.info("Details updated for employee with Id {} and old details are: {}", id, existingEmployee);
        return employeeRepository.save(employee);
    }

    /**
     * Checks if a certain employee exists in DB if not then throw Error.
     * @param employee
     */
    private void verifyIfEmployeeIsPresent(Optional<Employee> employee, Long id) {
        if(!employee.isPresent()){
            LOGGER.error("Employee with Id {} not found in DB.", id);
            throw new EmployeeNotFoundException(String.format(getMessage(EMPLOYEE_NOT_FOUND), id));
        }
    }

    /**
     * Validates department Id provided for a employee from department-service
     * @param id
     */
    private void validateDepartmentIdFromClient(Long id) {
        try {
            Department department = departmentClient.findById(id);
            departmentRedisRepository.save(department);
        } catch (Exception e){
            LOGGER.error("There was some error in finding the department with Id {}", id);
            throw new InvalidDepartmentException(getMessage(INVALID_DEPARTMENT));
        }
    }

    private String getMessage(String messageKey){
        return messageSource.getMessage(messageKey, null, Locale.ENGLISH);
    }
}
