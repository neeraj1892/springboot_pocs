package com.iris.employee.manager;

import com.iris.employee.model.Employee;

import java.util.List;

public interface EmployeeManager {
    Employee createEmployee(Employee employee);

    Employee findById(Long id);

    List<Employee> findAllEmployee();

    void deleteById(Long id);

    Employee updateEmployee(Employee employee, Long id);
}
