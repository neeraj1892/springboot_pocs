package com.iris.employee.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.Objects;

import static com.iris.employee.constants.EmployeeConstants.*;

@Data
@Getter
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = VALIDATION_DEPARTMENT_ID_NOT_EMPTY)
    @Min(value = 0L, message = VALIDATION_DEPARTMENT_ID_NOT_NEGATIVE)
    private Long departmentId;
    @NotBlank(message = VALIDATION_FIRST_NAME_NOT_EMPTY)
    private String firstName;
    @NotBlank(message = VALIDATION_LAST_NAME_NOT_EMPTY)
    private String lastName;
    @NotBlank(message = VALIDATION_PHONE_NUMBER_NOT_EMPTY)
    @Size(min = 10, max = 10, message =  VALIDATION_PHONE_NUMBER_SIZE)
    private String phoneNumber;
    @NotBlank(message = VALIDATION_ADDRESS_NOT_EMPTY)
    private String address;
    @CreationTimestamp
    private Date createdOn;
    @UpdateTimestamp
    private Date updatedOn;
}
