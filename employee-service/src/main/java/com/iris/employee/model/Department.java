package com.iris.employee.model;

import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.Date;


@Data
@RedisHash("Department")
public class Department implements Serializable {

    @javax.persistence.Id
    private Long id;
    private String deptName;
    private Date createdOn;
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
