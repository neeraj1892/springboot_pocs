package com.iris.employee.repository;

import com.iris.employee.model.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRedisRepository extends CrudRepository<Department, Long> {
}
