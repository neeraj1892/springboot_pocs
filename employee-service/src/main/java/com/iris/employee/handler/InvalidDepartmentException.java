package com.iris.employee.handler;

public class InvalidDepartmentException extends RuntimeException {

    public InvalidDepartmentException(String message) {
        super(message);
    }
}
