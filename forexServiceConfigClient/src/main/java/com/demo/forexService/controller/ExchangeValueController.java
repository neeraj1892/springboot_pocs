package com.demo.forexService.controller;

import com.demo.forexService.model.ExchangeValue;
import com.demo.forexService.repository.ExchangeValueRepository;
import com.demo.forexService.service.ExchangeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ExchangeValueController {

    @Autowired
    private ExchangeValueService service;

    @Value("${currency.from}")
    private String from;

    @Value("${currency.to}")
    private String to;

    @GetMapping("/currency-exchange")
    public ExchangeValue retrieveExchangeValue()
    {
        ExchangeValue exchangeValue=service.retrieveExchangeValue(from,to);
        return exchangeValue;
    }
}
