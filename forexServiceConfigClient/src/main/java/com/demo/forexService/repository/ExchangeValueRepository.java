package com.demo.forexService.repository;

import com.demo.forexService.model.ExchangeValue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeValueRepository extends JpaRepository<ExchangeValue,Integer> {

    ExchangeValue findByFromAndTo(String from,String to);
}
