package com.demo.forexService.service;

import com.demo.forexService.model.ExchangeValue;
import com.demo.forexService.repository.ExchangeValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExchangeValueService {

    @Autowired
    private ExchangeValueRepository repository;

    public ExchangeValue retrieveExchangeValue(String from,String to)
    {
        return repository.findByFromAndTo(from,to);

    }
}
