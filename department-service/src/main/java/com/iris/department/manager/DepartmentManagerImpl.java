package com.iris.department.manager;

import com.iris.department.constants.DepartmentConstants;
import com.iris.department.handler.DepartmentNotFoundException;
import com.iris.department.model.Department;
import com.iris.department.repository.DepartmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class DepartmentManagerImpl implements DepartmentManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentManagerImpl.class);

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    MessageSource messageSource;

    /**
     * Find department based on it's ID
     * @param id
     * @return
     */
    public Department findById(long id){
        Department department = departmentRepository.findById(id);
        verifyIfDepartmentIdPresent(department, id);
        LOGGER.info("Department details with Id {} are : {}", id, department);
        return department;
    }

    /**
     * Create details for a new department.
     * @param department
     * @return
     */
    public Department createDepartment(Department department){
        Department createdDepartment = departmentRepository.createDepartment(department);
        LOGGER.info("Details successfully saved for department : {}", department);
        return createdDepartment;
    }

    /**
     * Deletes a department details with specific Id,
     * If department is not found then throw error.
     * @param id
     */
    public void deleteDepartment(long id){
        Department department = findById(id);
        LOGGER.info("Deleting department with Id and details : {}", id, department);
        departmentRepository.deleteDepartment(department);
    }

    /**
     * Fethces a lost of all available departments in the DB.
     * @return
     */
    public List<Department> getAllDepartments() {
        LOGGER.info("Fetching list of all available departments.");
        return departmentRepository.getAllDepartments();
    }

    /**
     * Update a department's details based on it's Id,
     * if department does not exists then throw error.
     * @param department
     * @param id
     */
    public void updateDepartment(Department department, long id){
        Department existingDepartment = findById(id);
        department.setId(id);
        department.setCreatedOn(existingDepartment.getCreatedOn());
        LOGGER.info("Updating details for department with Id {} and old Details are : {}", id, existingDepartment);
        departmentRepository.updateDepartment(department);
    }

    /**
     * Checks if a department is present in database or not
     * If department is not present then throw error.
     * @param department
     */
    private void verifyIfDepartmentIdPresent(Department department, Long id) {
        if(department == null){
            LOGGER.error("Department with id {} not found in DB.", id);
            throw new DepartmentNotFoundException(String.format(getMessage(DepartmentConstants.DEPARTMENT_NOT_FOUND), id));
        }
    }

    private String getMessage(String messageKey){
        return messageSource.getMessage(messageKey, null, Locale.ENGLISH);
    }
}
