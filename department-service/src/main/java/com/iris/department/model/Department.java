package com.iris.department.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

import static com.iris.department.constants.DepartmentConstants.*;

@Data
@Entity
@NamedQuery(name = GET_ALL_DEPARTMENTS, query = GET_ALL_DEPARTMENTS_QUERY)
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = VALIDATION_DEPARTMENT_NAME_NOT_EMPTY)
    private String deptName;
    @CreationTimestamp
    private Date createdOn;
    @UpdateTimestamp
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
