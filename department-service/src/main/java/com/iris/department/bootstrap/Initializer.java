package com.iris.department.bootstrap;

import com.iris.department.model.Department;
import com.iris.department.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Initializer implements CommandLineRunner {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void run(String... args) throws Exception {
        Department department = new Department();
        department.setDeptName("Accounts");

        departmentRepository.createDepartment(department);
    }
}
