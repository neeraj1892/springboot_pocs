package com.example.springboot.client.CustomerService.controller;

import com.example.springboot.client.CustomerService.model.Customer;
import com.example.springboot.client.CustomerService.service.CustomerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/product")
public class CustomerControllerClient {

    @Autowired
    CustomerServices customerService;

    /**
     *
     * @return
     */
    @RequestMapping(value = "/customers/")
    public ResponseEntity<List<Customer>> listAllUsers() {

        List<Customer> customers = customerService.findAllCustomers();
        if (customers.isEmpty()) {
            return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
        } else
            return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> getUser(@PathVariable("id") String id) {
        System.out.println("Fetching User with id " + id);
        Customer customer = customerService.findById(id);
        if (customer == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Customer>(customer, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody Customer customer, UriComponentsBuilder uriBuilder) {
        System.out.println("Creating customer: " + customer.getCustomerName());

        if (customerService.isUserExists(customer)) {
            System.out.println("A Customer with name " + customer.getCustomerName() + " already exist");
        }
        customerService.saveCustomer(customer);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriBuilder.path("/customers/{id}").buildAndExpand(customer.getCustomerId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

}
