package com.example.springboot.client.CustomerService.model;

public class ProductPurchased {

    private String productID;
    private String productName;
    private String productDesc;

    public ProductPurchased()
    {

    }

    public ProductPurchased(String id,String name,String desc){
        productID=id;
        productName=name;
        productDesc=desc;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    @Override
    public String toString() {
        return "ProductPurchased{" +
                "productID='" + productID + '\'' +
                ", productName='" + productName + '\'' +
                ", productDesc='" + productDesc + '\'' +
                '}';
    }
}
