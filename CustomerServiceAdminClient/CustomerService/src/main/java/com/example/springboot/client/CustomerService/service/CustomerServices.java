package com.example.springboot.client.CustomerService.service;

import com.example.springboot.client.CustomerService.model.Customer;
import com.example.springboot.client.CustomerService.model.ProductPurchased;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class CustomerServices {

    private final List<Customer> customers;
    private final List<ProductPurchased> productList;
    private static final AtomicLong counter = new AtomicLong();

    public CustomerServices() {
        productList = new ArrayList<>();
        customers=new ArrayList<>();
        initializeProdutListWithDefaults();
        initializeCustomerListWithDefaults();
    }

    private void initializeCustomerListWithDefaults() {
        customers.add(new Customer("Cust1","Shilpa","9988776655","shilpa@gmail.com",productList));
    }

    private void initializeProdutListWithDefaults() {
        productList.add(new ProductPurchased("P1","Azure","This is microsoft SAS platform!"));
        productList.add(new ProductPurchased("P2","R Language","This is a business analytics tool!"));
        productList.add(new ProductPurchased("P3","Netflix Eureka","This is a Microservice Registry Server!"));
    }

    public List<Customer> findAllCustomers() {
        return customers;
    }

    public Customer findById(String id) {
        for (Customer customer : customers) {
            if (customer.getCustomerId().equalsIgnoreCase(id)) {
                return customer;
            }
        }
        return null;
    }

    public boolean isUserExists(Customer customer) {
        if (customers.contains(customer)) {
            return true;
        }
        return false;
    }

    public void saveCustomer(Customer customer) {
        customer.setCustomerId(new String("P")+counter.incrementAndGet());
        // add user using arraylist add method
        customers.add(customer);
    }

}

