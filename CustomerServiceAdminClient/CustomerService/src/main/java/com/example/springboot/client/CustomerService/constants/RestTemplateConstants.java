package com.example.springboot.client.CustomerService.constants;

public class RestTemplateConstants {

    public static final String URL_EMPLOYEES="http://localhost:8094/customers";
    public static final String URL_EMPLOYEES_JSON="http://localhost:8094/customers.json";

}
