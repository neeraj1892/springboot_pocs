package com.example.springboot.client.CustomerService.model;

import java.util.List;


public class Customer {

    private String customerId;
    private String customerName;
    private String customerContact;
    private String customerEmail;
    private List<ProductPurchased> productList;

    public Customer()
    {
    }

    public Customer(String customerId, String customerName, String customerContact, String customerEmail, List<ProductPurchased> productList) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerContact = customerContact;
        this.customerEmail = customerEmail;
        this.productList = productList;
    }

    public List<ProductPurchased> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductPurchased> productList) {
        this.productList = productList;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerContact() {
        return customerContact;
    }

    public void setCustomerContact(String customerContact) {
        this.customerContact = customerContact;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerContact='" + customerContact + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                ", productList='" + productList + '\'' +
                '}';
    }
}
